package org.yx.mongotest.lock;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author yangxin
 * @date 2021-10-21 10:45
 * @since v1.6.5
 */
public class Counter {    private int count = 0;

    public synchronized void add(int n) {
        if (n < 0) {
            dec(-n);
            System.out.println(count);
            return;
        }
        count += n;

        System.out.println(count);
    }

    public synchronized void dec(int n) {
        count += n;
    }

    public static void main(String[] args) {
        Counter counter = new Counter();

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setThreadGroupName("zhangsan");
        executor.setMaxPoolSize(5);
        executor.setKeepAliveSeconds(10);
        executor.initialize();


        executor.execute(() -> {
            counter.add(-10);
        });


    }


}
