package org.yx.mongotest.mqtt;

import lombok.SneakyThrows;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @author yangxin
 */
public class IMqttClient {

    @SneakyThrows
    public static void main(String[] args) {
        MqttClient publisher = new MqttClient("tcp://127.0.0.1:61613", "faceTestId");

        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        options.setUserName("admin");
        options.setPassword("password".toCharArray());
        publisher.connect(options);


        // 接收队列消息
/*        publisher.subscribe("aa", (topic, msg) -> {
            byte[] payload = msg.getPayload();
            System.out.println("aa：" + new String(payload));
        });
        publisher.subscribe("bb", (topic, msg) -> {
            byte[] payload = msg.getPayload();
            System.out.println("bb：" + new String(payload));
        });*/
        publisher.subscribe("mqtt/face/Rec", (topic, msg) -> {
            byte[] payload = msg.getPayload();
            System.out.println("faceTest：" + new String(payload));
        });


        // 向队列推送消息
   /*     MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setPayload("bbbbbbbbbbbb".getBytes());
        publisher.publish("bb", mqttMessage);*/
    }

}
